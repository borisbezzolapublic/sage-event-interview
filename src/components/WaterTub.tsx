import { FC } from "react";
import { useState } from "react";
import styled from "styled-components";
import useInterval from "../hooks/useInterval";

/**
 * Setting up an array in order to render more easily multiple levels of water. *
 * Furthermore, this way the max water level is dynamic
 */
const LEVELS = 5;
const arrayLevels = new Array(LEVELS).fill(0).map((_, ind) => ind + 1);
arrayLevels.reverse();

/* Type used to determine if the water level is currently still, rising or decreasing */
type StatusType = "nothing" | "increasing" | "decreasing";

const WaterTub: FC = () => {
    const [level, setLevel] = useState(0);

    const [status, setStatus] = useState<StatusType>("nothing");

    /* Using an hook to start an interval to increase or decrease water */
    useInterval(
        () => {
            if (status === "increasing") {
                increaseWater();
            } else if (status === "decreasing") {
                decreaseWater();
            }
        },
        // Enable interval only if status is increasing or decreasing
        status === "nothing" ? 0 : 2000,
    );

    /* Only increase if water level is below maximum. Otherwise, stop filling tub */
    const increaseWater = () => {
        if (level < LEVELS) {
            setLevel((current) => current + 1);
        } else {
            setStatus("nothing");
        }
    };

    /* Only increase if water level is above 0. Otherwise, stop emptying tub */
    const decreaseWater = () => {
        if (level > 0) {
            setLevel((current) => current - 1);
        } else {
            setStatus("nothing");
        }
    };

    const changeStatus = (newStatus: StatusType) => {
        if (status === newStatus) {
            setStatus("nothing");
        } else {
            setStatus(newStatus);
        }
    };

    return (
        <StyledContainer>
            <StyledWaterTub>
                {arrayLevels.map((val) => (
                    <StyledWaterLevel filled={val <= level} key={val} />
                ))}
                <StyledWaterCounter>
                    Water level: {level * 20}px
                </StyledWaterCounter>
            </StyledWaterTub>
            <StyledButton
                active={status === "increasing"}
                onClick={() => changeStatus("increasing")}
            >
                increaseWaterLevel
            </StyledButton>
            <StyledButton
                active={status === "decreasing"}
                onClick={() => changeStatus("decreasing")}
            >
                decreaseWaterLevel
            </StyledButton>
        </StyledContainer>
    );
};

const StyledWaterTub = styled.div`
    border: 2px solid black;
    border-top: none;
    height: ${() => `${20 * LEVELS}px`};
    width: 90vw;
    display: grid;
    grid-template: repeat(${() => LEVELS}, 20px) / 1fr;
    grid-column: 2 span;
    position: relative;
`;

const StyledWaterCounter = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const StyledWaterLevel = styled.div<{ filled: boolean }>`
    width: 90vw;
    height: 20px;
    background-color: ${({ filled }) => (filled ? "#09C3DB" : "white")};
`;

const StyledContainer = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 25px;
`;

const StyledButton = styled.button<{ active: boolean }>`
    height: 64px;
    border-radius: 40px;
    background-color: white;

    border: 2px solid ${({ active }) => (active ? "#09C3DB" : "black")};
    color: ${({ active }) => (active ? "#09C3DB" : "black")};
    text-decoration: ${({ active }) => (active ? "underline" : "none")};

    transition: all 0.25s;

    @media screen and (max-width: 350px) {
        grid-column: 2 span;
    }
`;

export default WaterTub;
