import styled, { createGlobalStyle } from "styled-components";
import WaterTub from "./components/WaterTub";

const App = () => {
    return (
        <StyledApp>
            <WaterTub />
            <GlobalStyle />
        </StyledApp>
    );
};

const StyledApp = styled.div`
    width: 100vw;
    display: grid;
    place-items: center;
    padding: 50px 0px;
`;

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }
`;

export default App;
