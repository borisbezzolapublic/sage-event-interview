# Sage Events Interview

The component (the whole App) is a bathtub div that fills with water when you press a increaseWaterLevel button. Starting with water level 0, once you press the button, every 2 seconds, a new blue-colored div is added inside the bathtub div (but is only 20 pixels high). After the level reaches the height of the div (100 pixels; or 5 levels), the water stops filling.
When you press a decreaseWaterLevel button, every 2 seconds, the water decreases by a div of the same height. The water counter shows the height of the water in the div.

The task is to implement the React component. Please take the time to write this as though it were a peer reviewed feature for production in a large react app, designed for scale and efficiency.

Once complete, please send us a link to the user facing component, hosted on Heroku or something similar, as well as the core files (via Github or Dropbox or Drive or the method of your choice).
